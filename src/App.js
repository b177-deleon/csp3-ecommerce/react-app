import React, { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { UserProvider } from './UserContext';

import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';
import Cart from './components/Cart.js';

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

import Books from './pages/Books';
import BookView from './components/BookView';

import AdminPage from './pages/AdminPage';
import AddBook from './components/Admin/AddBook';
import AllBooks from './components/Admin/AllBooks';
import AllOrders from './components/Admin/AllOrders';

import './App.css';

export default function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch('https://csp3-ecommerce.herokuapp.com/users/details', {
      headers: {Authorization: `Bearer ${localStorage.getItem('token')}`}
    })
    .then(res => res.json ())
    .then(data => {
      if(typeof data._id !== 'undefined') {
        setUser({ id: data._id, isAdmin: data.isAdmin});
      } else {
        setUser({id: null, isAdmin: null})
      }
    })
  }, [])

  return (
    <div style={{ fontFamily: 'Bitter' }}>
          <UserProvider value={{user, setUser, unsetUser}}>
            <Router>
              <AppNavbar />
                <Container>
                  <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/register" element={<Register />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/logout" element={<Logout />} />
                    <Route path="*" element={<Error />} />

                    <Route path="/books" element={<Books />} />
                    <Route path="/books/:bookId" element={<BookView />} />

                    <Route path="/admin" element={<AdminPage/>} />
                    <Route path="/books/addBook" element={<AddBook/>} />
                    <Route path="/books/all" element={<AllBooks/>} />
                    <Route path="/users/orders" element={<AllOrders/>} />

                    <Route path="/cart" element={<Cart/>} />

                  </Routes>
                </Container>
              <Footer />
            </Router>
          </UserProvider>
    </div>
    
  );
}