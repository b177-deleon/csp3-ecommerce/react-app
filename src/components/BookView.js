import React, { useEffect, useState, useContext } from "react";
import { Container, Card, Button } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import UserContext from "../UserContext";

export default function BookView(){

    const { bookId } = useParams();

    const { user } = useContext(UserContext);

    const [title, setTitle] = useState("");
    const [author, setAuthor] = useState("");
    const [image, setImage] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);

    useEffect(() => {
        fetch(`https://csp3-ecommerce.herokuapp.com/books/${bookId}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setTitle(data.title);
            setAuthor(data.author);
            setImage(data.image);
            setDescription(data.description);
            setPrice(data.price);
        })
    }, [bookId]);

    return(
        <Container style={{
            marginTop: '100px',
            marginBottom: '100px',
            textAlign: 'center',
            fontFamily: 'Bitter',
            color: '#5D4632'
        }}>
            <Container>
                <img
                src={image}
                height="700px"
                width="500px"
                alt="Book cover"
                style={{ borderRadius: '20px' }}
                />
            </Container>
            <Card style={{ 
                border: "0px",
                paddingTop: '10px',
                marginTop: '20px',
                backgroundColor: 'transparent'
                }}>
                <Card.Body >
                    <Card.Title style={{ fontWeight: '800' }}>{title}</Card.Title>
                    <Card.Subtitle style={{ paddingBottom: '10px' }}>by {author}</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Text>Php{price}</Card.Text>

                {user.isAdmin ?
                    <Button style={{ 
                        backgroundColor: 'transparent',
                        color: 'rgb(93, 70, 50)',
                        borderColor: 'rgb(93, 70, 50)' }} 
                        as={Link} to='/books/all'>Go back
                    </Button>
                    :
                    <Button style={{ 
                        backgroundColor: 'transparent',
                        color: 'rgb(93, 70, 50)',
                        borderColor: 'rgb(93, 70, 50)' }} 
                        as={Link} to='/books'>Go back
                    </Button>
                }
                    
                </Card.Body>
            </Card>
        </Container>
    )
}