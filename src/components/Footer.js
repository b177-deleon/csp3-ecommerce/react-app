import React from "react";
import { MDBContainer, MDBRow, MDBFooter } from 'mdb-react-ui-kit';


export default function Footer() {
    return (
        <MDBFooter style={{ 
          backgroundColor: 'rgb(93, 70, 50)',
          color: 'white',
          fontWeight: '500',
          fontFamily: 'Bitter',
          textAlign: 'center',
          marginTop: '60px' }}>
          <MDBContainer className='pt-4 pr-4 pl-4 pb-0'>
            <section className=''>
              <MDBRow lg='3' md='6' className='mb-4 mb-md-0'>
                  <h5 className='text-center'>Contact Us</h5>
    
                  <ul className='text-center list-unstyled mb-0'>
                    <li>
                      <p>yhajedeleon@gmail.com</p>
                    </li>
                    <li>
                      <p>+63 945-115-4135</p>
                    </li>
                  </ul>
              </MDBRow>
            </section>
          </MDBContainer>
    
          <div className='text-center p-3' style={{ backgroundColor: 'rgb(82, 62, 44)' }}>
            &copy; {new Date().getFullYear()} made by Yhaje de Leon
          </div>
        </MDBFooter>
    );
}