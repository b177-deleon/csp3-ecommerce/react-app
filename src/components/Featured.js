import React from "react";
import { Row } from 'react-bootstrap';
import { Link } from "react-router-dom";
import { Grid } from "@mui/material";

export default function Featured(){
    return (
        <Row >
            <h1 style={{
				fontFamily: 'Bitter',
				color: '#5D4632',
				textAlign: 'center',
				fontWeight: '600',
				fontSize: '60px',
				paddingBottom: '20px',
			}}>Featured Books</h1>
				<Grid 
					container 
					direction="row"
					justifyContent="space-evenly"
					alignItems="center" 
					columns={{ md: 12 }}
					>
					<Grid md={3}>
						<Link to="books/62d15e60d26ce6ced17bd5a4">
							<img
							src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1541887843l/40495957._SY475_.jpg"
							alt="Heartstopper Volume #1"
							style={{
								borderRadius: '50px'
							}}
							/>
						</Link>
					</Grid>
					<Grid md={3}>
						<Link to="books/62d16893d26ce6ced17bd5b2">
							<img
							src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1400602609l/28187.jpg"
							alt="The Lightning Thief"
							style={{
								borderRadius: '50px'
							}}
							/>
						</Link>
					</Grid>
					<Grid md={3}>
						<Link to="books/62d15afcd26ce6ced17bd599">
							<img
							src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1500930947l/9969571._SY475_.jpg"
							alt="Ready Player One"
							style={{
								borderRadius: '50px'
							}}
							/>
						</Link>
					</Grid>
				</Grid>
			</Row>
    )
}