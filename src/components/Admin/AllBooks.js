import React, { Fragment, useEffect, useState } from 'react';
import { Container, Table, Button, Row, Form, Modal } from 'react-bootstrap';
import { Input, InputAdornment } from '@mui/material';
import { Link } from "react-router-dom";
import SearchTwoToneIcon from '@mui/icons-material/SearchTwoTone';
import Swal from 'sweetalert2';

export default function AllBooks(){

	const [books, setBooks] = useState([]);
	const [searchTerm, setSearchTerm] = useState("");

    const [showUpdate, setShowUpdate] = useState(false);

    const [id, setId] = useState("");
    const [title, setTitle] = useState("");
    const [author, setAuthor] = useState("");
    const [image, setImage] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [stock, setStock] = useState("");

	const fetchAllBooks = () => {
		fetch('https://csp3-ecommerce.herokuapp.com/books/all', {
            headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
        })
		.then(res => res.json())
		.then(data => {
			setBooks(data)
		})
	}

	useEffect(() => {
			fetchAllBooks()
	}, []);

    const openUpdate = (_id) => {

		fetch(`https://csp3-ecommerce.herokuapp.com/books/${_id}`)
		.then(res => res.json())
		.then(data => {
            setId(data._id);
			setTitle(data.title);
            setAuthor(data.author);
            setImage(data.image);
			setDescription(data.description);
			setPrice(data.price);
            setStock(data.stock);
		});

		setShowUpdate(true);

	};

    const closeUpdate = () => {
        setTitle("");
        setAuthor("");
        setImage("");
        setDescription("");
        setPrice(0);
        setStock("");
        setShowUpdate(false);
    }

    const updateBook = (e, _id) => {

		e.preventDefault();

		fetch(`https://csp3-ecommerce.herokuapp.com/books/${_id}/update`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				title: title,
                author: author,
                image: image,
				description: description,
				price: price,
                stock: stock
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {
                Swal.fire({
                    title: 'Updated!',
                    icon: 'success'
                });
                fetchAllBooks();
                setTitle("");
                setAuthor("");
                setImage("");
				setDescription("");
				setPrice(0);
                setStock("");
                closeUpdate();

			} else {
				Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error'
                });
                closeUpdate();
            }
        })
    }

    const archiveBook = (_id) => {
        fetch(`https://csp3-ecommerce.herokuapp.com/books/${_id}/archive`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`,
            }
        })
        .then(res => res.json())
        .then(data => {
            
            if (data === true) {
                fetchAllBooks();
                Swal.fire({
                    title: 'Archived!',
                    icon: 'success'
                });
            } else {
                Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error'
                });
            }

        });

    };

    const activateBook = (_id) => {
        fetch(`https://csp3-ecommerce.herokuapp.com/books/${_id}/activate`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`,
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                fetchAllBooks();
                Swal.fire({
                    title: 'Activated!',
                    icon: 'success'
                });
            } else {
                Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error'
                });
            }

        });

    };

    const deleteBook = (_id) => {
        fetch(`https://csp3-ecommerce.herokuapp.com/books/${_id}/delete`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`,
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                fetchAllBooks();
                Swal.fire({
                    title: 'Deleted!',
                    icon: 'success'
                });
            } else {
                Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error'
                });
            }

        });

    };

	return(
		<Fragment>

            <h1 style={{ 
                fontSize: '50px',
                color: '#5D4632',
                textAlign: 'center',
                borderColor: '#5D4632',
                marginTop: '20px',
                fontWeight: '800' }} 
            >All Books</h1>

            <div className='text-center'>
                <p>Search by book title or author:</p>
            </div>

			<Container style={{
				display: 'flex',
				justifyContent: 'center',
				marginTop: '20px'
			}}>
		
			<Input
				style={{
					height: '50%',
					width: '50%',
					marginTop: '10px',
					marginBottom: '40px',
                    fontFamily: 'Bitter',
                    color: '#5D4632'
				}}
				type="text"
				placeholder="Looking for something?"
				onChange={(e) => {
					setSearchTerm(e.target.value);
				}}
				startAdornment={
					<InputAdornment position="start">
						<SearchTwoToneIcon />
					</InputAdornment>
				}
				/>
			</Container>
			
            <Table responsive
                style={{ 
                    fontFamily: 'Bitter',
                    color: '#5D4632',
                    textAlign: 'center',
                    borderColor: '#5D4632',
                    minWidth: 500 }}
                className="table table-hover">
                    <thead
                        style={{
                            backgroundColor: '#5D4632',
                            color: 'white'
                        }}>
                        <tr>
                            <th>Book ID</th>
                            <th>Book Title</th>
                            <th>Author</th>
                            <th>Price</th>
                            <th>Stocks</th>
                            <th>Availability</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {books.filter((book) => {
                            if (searchTerm === "") {
                                return books;
                            } 
                            else if (book.title.toLowerCase().includes(searchTerm.toLowerCase())) {
                                return book;
                            }
                            else if (book.author.toLowerCase().includes(searchTerm.toLowerCase())) {
                                return book;
                            }
                        })
                        .map((book) => {
                            return (
                                <tr key={book._id}>
                                    <td>{book._id}</td>
                                    <td><Link to={`/books/${book._id}`}>{book.title}</Link></td>
                                    <td>{book.author}</td>
                                    <td>Php {book.price}</td>
                                    <td>{book.stock}</td>
                                    <td>
                                        {book.isActive ?
                                            <span>Available</span>
                                        :
                                            <span>Out of Stock</span>
                                        }
                                    </td>
                                    <td>
                                        <Button 
                                            style={{ 
                                                backgroundColor: 'transparent',
                                                color: 'blue',
                                                borderColor: 'blue' }} 
                                            size="sm" 
                                            onClick={() => openUpdate(book._id)}
                                        >
                                            Edit
                                        </Button>
                                        {book.isActive ?
                                            <Button 
                                                style={{ 
                                                    backgroundColor: 'transparent',
                                                    color: 'red',
                                                    borderColor: 'red',
                                                    marginRight: '10px',
                                                    marginLeft: '10px' }} 
                                                size="sm"
                                                onClick={() => archiveBook(book._id)}
                                            >
                                                Disable
                                            </Button>
                                        :
                                            <Button
                                            style={{ 
                                                backgroundColor: 'transparent',
                                                color: 'green',
                                                borderColor: 'green',
                                                marginRight: '10px',
                                                marginLeft: '10px' }} 
                                                size="sm"
                                                onClick={() => activateBook(book._id)}
                                            >
                                                Enable
                                            </Button>
                                        }
                                        <Button 
                                            style={{ 
                                                backgroundColor: 'transparent',
                                                color: 'gray',
                                                borderColor: 'gray' }} 
                                            size="sm" 
                                            onClick={() => deleteBook(book._id)}
                                        >
                                            Delete
                                        </Button>
                                    </td>
                                </tr>
                                )
                        })}
                    </tbody>
                </Table>

                <Modal 
                    show={showUpdate} 
                    onHide={closeUpdate}
                    style={{ 
                        fontFamily: 'Bitter',
                        color: '#5D4632' }}>
                    <Form onSubmit={e => updateBook(e, id)}>
                        <Modal.Header closeButton>
                            <Modal.Title style={{ 
                                fontSize: '50px' }}>Edit Book</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Row>
                                <Form.Group className="mb-3 w-50" controlId="bookTitle">
                                    <Form.Label>Title</Form.Label>
                                    <Form.Control 
                                        type="string"
                                        placeholder="Enter book title" 
                                        value={title}
                                        onChange={e => setTitle(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group className="mb-3 w-50" controlId="bookAuthor">
                                    <Form.Label>Author</Form.Label>
                                    <Form.Control 
                                        type="string"
                                        placeholder="Enter author's name" 
                                        value={author}
                                        onChange={e => setAuthor(e.target.value)}
                                        required
                                    />
                                </Form.Group>
                            </Row>
                            <Row>
                                <Form.Group className="mb-3 w-100" controlId="bookImage">
                                    <Form.Label>Image</Form.Label>
                                    <Form.Control 
                                        type="string"
                                        placeholder="Enter image link" 
                                        value={image}
                                        onChange={e => setImage(e.target.value)}
                                        required
                                    />
                                </Form.Group>
                            </Row>
                            <Row>
                                <Form.Group className="mb-3 w-100" controlId="bookDescription">
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control 
                                        type="string"
                                        placeholder="Enter book description" 
                                        value={description}
                                        as="textarea" rows={5}
                                        onChange={e => setDescription(e.target.value)}
                                        required
                                    />
                                </Form.Group>
                            </Row>
                            <Row>
                                <Form.Group className="mb-3 w-50" controlId="price">
                                    <Form.Label>Price</Form.Label>
                                    <Form.Control 
                                    type="currency" 
                                    placeholder="Price" 
                                    value={price}
                                    onChange={e => setPrice(e.target.value)}
                                    required
                                    />
                                </Form.Group>

                                <Form.Group className="mb-3 w-50" controlId="stock">
                                    <Form.Label>Stock</Form.Label>
                                    <Form.Control 
                                        type="number" 
                                        placeholder="Stock" 
                                        value={stock}
                                        onChange={e => setStock(e.target.value)}
                                        required
                                    />
                                </Form.Group>
                            </Row>

                        </Modal.Body>
                        <Modal.Footer>
                            <Button style={{ 
                                backgroundColor: 'transparent',
                                color: 'red',
                                borderColor: 'red',
                                fontWeight: '800',
                                marginRight: '10px',
                                marginLeft: '10px' }}
                                onClick={closeUpdate}>
                                Cancel
                            </Button>

                            <Button style={{ 
                                backgroundColor: 'transparent',
                                color: 'green',
                                borderColor: 'green',
                                fontWeight: '800',
                                marginRight: '10px',
                                marginLeft: '10px' }} type="submit">
                                Submit
                            </Button>
                        </Modal.Footer>
                    </Form>	
                </Modal>

		</Fragment>
	)
}
