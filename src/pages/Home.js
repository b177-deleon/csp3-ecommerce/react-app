import React, { Fragment } from 'react';
import { Row, Col, Button, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import youngAdult from '../assets/young-adult.jpg';
import Featured from '../components/Featured';


export default function Home(){

	return (
		<Fragment>
			<Row>
				<Col className="banner">
					<Container className="mx-auto d-none d-md-block">
						<img
						src={youngAdult}
						alt="Homepage"
						style={{
							width: '100%',
							maxWidth: '700px',
							height: 'auto',
							borderRadius: '50px'
						}}
						/>
					</Container>
					<Container>
						<h1 className="title">BookTales</h1>
						<h3 className="pb-4">Are you ready to embark on an unforgettable adventure?</h3>
						<Button style={{ 
                                    backgroundColor: 'transparent',
                                    color: 'rgb(93, 70, 50)',
                                    textTransform: 'capitalize',
									fontWeight: '800',
                                    borderColor: 'rgb(93, 70, 50)' }} as={Link} to="/books">Let's go!
						</Button>
					</Container>
				</Col>
			</Row>

			<Featured />
			
		</Fragment>
	)
}